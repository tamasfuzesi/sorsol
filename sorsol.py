# -*- coding: utf-8 -*-
__author__ = "Pato"
import random, os, time
import wx
import wx.html


from wx.lib.embeddedimage import PyEmbeddedImage

icon = PyEmbeddedImage(
    "iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABHNCSVQICAgIfAhkiAAABkpJ"
    "REFUWIWtV1tsXEcZ/v6Zc9vjPbvetZ3UbdOWpkqIY0uR2opCE6UJfUC0fWnjVAKhohS5UtQg"
    "IcELDxgD4oGCQLTKjWtfEI1LRQtK1Fapq9Q0KuCqmNgRqRVo5eDbete7e87uuc0MD+4Sb3bt"
    "XRe+p6O5fP93/vln5hvC/4DBM+CZzEBfGMtDQcDuE7HqZEz5uk7vmyZeY3r4xukDV3IbcdDH"
    "Dq4GeerV6UOlIn23WGbbRURMKYAIYAwwTFFNdaq37Q58r1SYemv0MEQzHq1Z4/ClPmNuzk9o"
    "miWWlnZXRw+PNkzOjk3ft1RgPyiusNuUut6uFCAEUK3wRBjgs046vjOT3fW0UpfPEUHdyFOX"
    "ga+93ZctuvRQGNBDUUC3gSM0DTmpG3jJvj24+OyOmQAAnhi73ZJe6ufzs+wLQm6cRcaAri75"
    "t86eePDUA5ffb+ivfQyN9X9yYZH/YnGOn57/N388t8Q+nZtn++dn+dPLSzRanDG+MzS2oxsA"
    "LLK3VVzsbRUcAKQEVoo04FfZF4fV9Xg1cAA4NjZwa6GgTuVy7HOhT/ralEpFFPjMDnx2r6Gz"
    "nvuPbh0XodxVLNCRKGJGKwEAoCSRacDQFnp/987zC/7aPm3o1N26W/WPFvLsgIioQWENgU9G"
    "fpl9iRviA87oqlBoKziwWhdRjK1VLUoBWFnbx9hd4Z3lEh6PIsZbEVUrZJby7CgRPtVu8OuR"
    "EEDKqKE5iOXeSpW2NdZnc5RL7KYgYIeJGtdzPXAOJCz5nunF+QYBcYi9Ucj1dsmEAHllusk0"
    "qWXGgNVzIZmSC7Ypf/ns51d3UZ2AMGA7pWw3/Co8l2H9allDzoBkOl7MdKlvI2tfaDomimVW"
    "tZn+GuIIcEvNFRADdEMh6Sj09MZRVzf9GQDUSuXBY+O7tw9f6qsrXnrstwMzhTzbvjkJzZGw"
    "FTqSq38T+IQoAqSAApTkmgoTlpqzHPa6ZYuTt7w5PTkyAkmHzuz+ez6n9f8/BPCPDnYpVrde"
    "M2hcKScrrmQ68fVbxqfOaqZFVxhHv2x6VWwOIm49JhZExWVtJxD/UN/X/yFLWPLFhC28VhOJ"
    "ANNSsDsU6GPfoauQEnALfEclwpf5nqeyH5iMp4VUA0Iwo5a62rWq60olHZCTVmB8dW2FaFSw"
    "WVFKEWkGdAKAI+M7HRbwhz2XPSoi+oRQ0tCIuYalljRd3lHxWH+pwFgQEm48sDQN0DQF3QDc"
    "Mq279s3QmZEzdbqPnb3LlLqWFLbBtRgdbkV+NZejJ6suc9YjTqaltC3pA8x2XULFaz8V2e74"
    "UtPRQ6/fnfbD4PvLi+zJapXMjUhsW1R7t8nnS3k+yHV05RYZ4jaKkXNgS68cbThNhsf2a3Hs"
    "Dy3n2JFWwQFAgTGlcD6RVCejWEZOWrZVD5YpvISlXmwQMEf5XcUSHfUrZLWmAaQEMbDYtuRz"
    "XKdxRkAqvfFO0bhS6QzOsaz5ar0ABQqr6mHXrfd5G4FzyFghOn5get7pEM8IgbzjxF42Gy9q"
    "vJFFM5TMbFUXzIT61ul7Jop1pnR4qk+/Esp7RcTbvmoZVKhBFgEgaZff9JPpVzyXP9a1RTxn"
    "JcWtXpkdjANsAQDdwlLSUX+0HfrpiX1TV4EbXPHc1YSmhL9uxTeDZqJoJY0FAPjxZ2arQ+ed"
    "X1Vc9ojnsT2pDH0lnaTOSIibFSkymHYt7DBmT9wz8V9jUm/L5xDxm2mOsVVr3QpEgKnLf/m6"
    "sVhrc7r0Ca8sL1Y92p9Myr4TB6f+BOCf63HUpfr0UxORZcnzhqkajEPTv+dKmTZd6P3DRLnW"
    "9qM9k55hqbORUCm/yh5p5oTXFQAAuq2dc1LiLc5am7QOR80bFn4/MoI6S6Ob9BciKvmB3D97"
    "sa9zUwJO3j+56Dj4ZrZbvtusimuwEiJKp+Wve5fkZCNrfE03sOBX2R2mUL2bEgAAP3tw6q9d"
    "mfiJnl75gt2hXL7G/TEG2B3K7+7Bb0zOfzJyeDq8cX6aK1cjlVcKtojYhhlo+jYEQR3H5alv"
    "jO8cSiTZQd+lQ0HIdktFpqnhmmWpl8wkvXBi32Sh2fQ5z5Scy1DjqsyVXGk2ZmMBH+GZvf8o"
    "A3h58EzfuUxPmNJsTUMF7vEHpj00eWjWsL0YRx920jtmAm/wIJrZKMZ/AO6ywqlSyi3TAAAA"
    "AElFTkSuQmCC")

class HelpDialog(wx.Dialog):
    def __init__(self, parent):
        wx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=wx.EmptyString, pos=wx.DefaultPosition,
                           size=wx.DefaultSize, style=wx.DEFAULT_DIALOG_STYLE)

        self.SetSizeHintsSz(wx.DefaultSize, wx.DefaultSize)

        bSizer2 = wx.BoxSizer(wx.VERTICAL)

        bSizer2.SetMinSize(wx.Size(300, 200))
        self.html_box = wx.html.HtmlWindow(self, wx.ID_ANY, wx.DefaultPosition, wx.Size(-1, 200),
                                           wx.html.HW_SCROLLBAR_AUTO)
        self.html_box.SetPage(u'<b>Használat:</b><br>'
                              u'<ol>'
                              u'<li>.txt fájlba egymás alá elhelyezni a sorsolandó elemeket</li>'
                              u'<li>Programban kiválasztani az elmentett fájlt</li>'
                              u'<li>Beállítani a húzások számát</li>'
                              u'<li>A jelölőnégyzetet kipipálni, ha egy elem többször szerepelhet</li>'
                              u'<li>SORSOL! gombra klikkelni</li>'
                              u'</ol>'
                              u'<br><br><b>Készítette:</b> Füzesi Tamás<br>[tamas.fuzesi@outlook.com]')
        self.html_box.SetStandardFonts()
        bSizer2.Add(self.html_box, 0, wx.ALL | wx.EXPAND, 5)

        bSizer2.AddSpacer((0, 0), 1, wx.EXPAND, 5)

        self.btn_close = wx.Button(self, wx.ID_ANY, u"Bezár", wx.DefaultPosition, wx.DefaultSize, 0)
        bSizer2.Add(self.btn_close, 0, wx.ALIGN_RIGHT | wx.ALL | wx.EXPAND, 5)

        self.SetSizer(bSizer2)
        self.Layout()
        bSizer2.Fit(self)

        self.Centre(wx.BOTH)

        self.btn_close.Bind(wx.EVT_BUTTON, self.CloseDialog)

    def CloseDialog(self, Event):
        self.Destroy()

    def __del__(self):
        pass


class LotteryFrame(wx.Frame):
    def __init__(self, parent):
        wx.Frame.__init__(self, parent, id=wx.ID_ANY, title=u"Sorsoló Program (by Pato) v1.2", pos=wx.DefaultPosition,
                          size=wx.Size(-1, -1),
                          style=wx.CAPTION | wx.CLOSE_BOX | wx.DEFAULT_FRAME_STYLE | wx.MAXIMIZE_BOX | wx.MINIMIZE_BOX | wx.RESIZE_BORDER | wx.SYSTEM_MENU | wx.CLIP_CHILDREN | wx.TAB_TRAVERSAL)

        self.winners = []

        self.SetIcon(icon.GetIcon())

        self.SetSizeHintsSz(wx.Size(380, -1), wx.Size(-1, 380))
        self.SetBackgroundColour( wx.Colour( 244, 244, 244 ) )

        main_sizer = wx.BoxSizer(wx.VERTICAL)

        fgSizer1 = wx.FlexGridSizer(0, 2, 0, 0)
        fgSizer1.SetFlexibleDirection(wx.BOTH)
        fgSizer1.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_SPECIFIED)

        self.m_staticText1 = wx.StaticText(self, wx.ID_ANY, u"Fájl:", wx.DefaultPosition, wx.Size(50, -1), 0)
        self.m_staticText1.Wrap(-1)
        fgSizer1.Add(self.m_staticText1, 1, wx.ALIGN_CENTER_VERTICAL | wx.ALL, 5)

        self.fp_file = wx.FilePickerCtrl(self, wx.ID_ANY, wx.EmptyString, u"Select a file", u"*.txt",
                                         wx.DefaultPosition, wx.Size(300, -1),
                                         wx.FLP_DEFAULT_STYLE | wx.FLP_FILE_MUST_EXIST | wx.FLP_OPEN | wx.FLP_SMALL | wx.FLP_USE_TEXTCTRL)
        fgSizer1.Add(self.fp_file, 0, wx.ALIGN_RIGHT | wx.ALL | wx.EXPAND, 5)

        self.m_staticText4 = wx.StaticText(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText4.Wrap(-1)
        fgSizer1.Add(self.m_staticText4, 0, wx.ALL, 5)

        self.sc_lottery_num = wx.SpinCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size(150,-1),
                                          wx.SP_ARROW_KEYS, 1, 10, 1)
        self.sc_lottery_num.SetToolTipString(u"Húzások száma")

        fgSizer1.Add(self.sc_lottery_num, 0, wx.ALL, 5)

        self.m_staticText2 = wx.StaticText(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_staticText2.Wrap(-1)
        fgSizer1.Add(self.m_staticText2, 0, wx.ALL, 5)

        self.cb_more_win = wx.CheckBox(self, wx.ID_ANY, u"Egy elem többször nyerhet", wx.DefaultPosition,
                                       wx.DefaultSize, 0)
        fgSizer1.Add(self.cb_more_win, 0, wx.ALL, 5)

        main_sizer.Add(fgSizer1, 1, wx.EXPAND, 5)

        self.tc_winners = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size(-1, 150),
                                      wx.TE_MULTILINE | wx.TE_READONLY)
        main_sizer.Add(self.tc_winners, 0, wx.ALL | wx.EXPAND, 5)

        bSizer5 = wx.BoxSizer(wx.HORIZONTAL)

        self.btn_lottery = wx.Button(self, wx.ID_ANY, u"SORSOL!", wx.DefaultPosition, (150, -1), 0)
        bSizer5.Add(self.btn_lottery, 0, wx.ALL, 5)

        self.btn_help = wx.Button(self, wx.ID_ANY, u"Súgó", wx.DefaultPosition, wx.DefaultSize, 0)
        bSizer5.Add(self.btn_help, 0, wx.ALL, 5)

        main_sizer.Add(bSizer5, 0, 0, 5)

        self.SetSizer(main_sizer)
        self.Layout()
        main_sizer.Fit(self)

        self.Center()


        # Events #######################
        self.btn_help.Bind(wx.EVT_BUTTON, self.show_help_dialog)
        self.btn_lottery.Bind(wx.EVT_BUTTON, self.DoLottery)

    # Actions ######################
    def show_help_dialog(self, Event):
        help_dialog = HelpDialog(self)
        help_dialog.Show()

    def DoLottery(self, Event):
        self.tc_winners.Clear()
        lottery_file = self.fp_file.GetTextCtrlValue()
        del (self.winners[0:])
        if os.path.exists(lottery_file):
            self.lines = [line.rstrip('\n') for line in open(lottery_file)]
            if len(self.lines) > self.sc_lottery_num.GetValue():
                for i in range(self.sc_lottery_num.GetValue()):

                    if self.cb_more_win.GetValue():
                        self.tc_winners.AppendText(self._lottery() + '\n')
                    else:
                        wx.BeginBusyCursor()
                        wx.MilliSleep(3000)
                        while 1:
                            lot = self._lottery()
                            if not self._in_winners(lot):
                                self.winners.append(lot)
                                self.tc_winners.AppendText(lot + '\n')
                                break
                        wx.EndBusyCursor()
            else:
                wx.MessageBox(u'Kevesebb húzási lehetőséget kell beállítania!', 'Hiba',
                              wx.OK | wx.ICON_ERROR)
        else:
            wx.MessageBox(u'A fájlt nem sikerült elérni!', 'Hiba',
                          wx.OK | wx.ICON_ERROR)

    def _in_winners(self, item):
        if item in self.winners:
            return True
        return False

    def _lottery(self):
        return random.choice(self.lines)

    def __del__(self):
        pass


class LotteryApp(wx.App):
    def OnInit(self):
        frame = LotteryFrame(None)
        self.SetTopWindow(frame)
        frame.Show(True)
        return True


if __name__ == "__main__":
    app = LotteryApp()
    app.MainLoop()
